import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
  providers: [RestService]
})
export class StatisticsComponent implements OnInit {

  private statistics : any;

  constructor(private restService : RestService) {
      this.restService.getStatistics().subscribe(data => {
        this.statistics = data;
        console.log(this.statistics);
      });
  }

  ngOnInit() {
  }


}

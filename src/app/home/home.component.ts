import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [RestService]
})

export class HomeComponent implements OnInit {

  private image : any;
  private city : any;
  private force : any;



  constructor(private restService : RestService) {
    setInterval(()=>{
      var ipAdd = 'energetic-api.serveo.net';
      this.restService.getImage(ipAdd).subscribe(data => {
        console.log(data)
        // let imageServer = '172.16.32.72:8080';
        
        try {

        if('image' in data){
          var imageUrl = data['image'];
          this.image = 'http://' + ipAdd + imageUrl;
          console.log(this.image);
        }
        if('city' in data){
          this.city = data['city'];
        }
        if('force' in data){
          this.force = data['force'];
        }
      }  catch(e) {
        console.log(e)
    }


      });
    }, 2000);
  }

  ngOnInit() {
  }

}

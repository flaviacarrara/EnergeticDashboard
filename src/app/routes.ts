import {HomeComponent} from './home/home.component';
import {StatisticsComponent} from './statistics/statistics.component';
import { Routes } from "@angular/router";

export const routes: Routes = [
 { path: '', redirectTo: 'home', pathMatch: 'full' },
 { path: 'home', component: HomeComponent },
 { path: 'statistics', component: StatisticsComponent }
];
